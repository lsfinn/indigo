# README #

LIGO India Orientation Studies

### CONFIGURATION AND USE ###

* Git-flow for feature, fix, etc. 
* Code Directory: all software
* Notes Directory: technical notes & memoranda
* Manuscript: LaTeX and figure files

### Contribution guidelines ###

* Code
** All new code and associated documentation will be developed in an appropriately titled feature branch
** All code includes documentation describing exactly what it expects as input and what it should produce at output
** All code verifies its inputs
** All code is accompanied by code tests using matlab testing framework
** No feature branches are merged into development branch before code review
* Notes: 
** All technical notes and memoranda will be composed in an appropriately titled feature branch
** Technical notes associated with code will be composed in the same branch as the code
** Technical notes not associated with code will be developed in their own feature branches
** All technical notes and memoranda will be written using LaTeX, making use of appropriate templates
* Manuscript: All manuscript development will take place in its own feature branch, separate from code and technical notes. 